(function($) {
  $.entwine('ss', function($){
      // Dynamically change options
      $('select[name="CopyToSubsiteID"]').entwine({
          onadd:function(){
              this.syncOptions();
          },
          onchange:function(){
              this.syncOptions();
          },
          syncOptions:function(){
              var select = $('select[name="CopyToSubsiteParentID"]');
              var subsiteID = parseInt(this.val()) + '';
              var options = select.data('options');
              var current = select.prop('options');

              // If we don't have the extra options, we don't need to decorate the select
              if (options === null || typeof options !== 'object') {
                  return;
              }

              // Find new options
              var source = [];
              if (options.hasOwnProperty(subsiteID)) {
                  source = options[subsiteID];
              }

              // Remove existing options
              $('option', select).remove();

              // Add new options
              $.each(source, function(val, label) {
                  current[current.length] = new Option(label, val);
              });

              // Tell UI to update options
              select.trigger('chosen:update');
          }
      });
  });
}(jQuery));
