<?php

namespace Solnet\Blog;

use SilverStripe\Blog\Model\BlogPost;
use SilverStripe\Dev\Debug;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\DataExtension;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class BlogPostExtension extends DataExtension
{

    private static $db = [
        'Content' => 'HTMLText'
    ];

    private static $has_one = [
        'Avatar' => 'SilverStripe\Assets\Image',
    ];

    private static $many_many = [
        'RelatedPosts' => 'SilverStripe\Blog\Model\BlogPost'
    ];

    private static $many_many_extraFields = array(
        'RelatedPosts' => array('SortOrder' => 'Int')
    );

    public function updateCMSFields(FieldList $fields)
    {
        // If Elemental module has removed the content area, put it back
        if ($fields->dataFieldByName('ElementalArea') && !$fields->dataFieldByName('Content')) {
            $fields->removeByName(array('ElementalArea'));
            $fields->addFieldsToTab(
                'Root.Main',
                array(
                    HTMLEditorField::create(
                        'Content',
                        _t('BlogExtras.Content_Title', 'Content')
                    )
                ),
                'CustomSummary'
            );
        }

        $fields->removeByName(array('RelatedPosts'));

        $fields->addFieldsToTab(
            'Root.PostOptions',
            [
                $imageUpload = UploadField::create(
                    'Avatar',
                    _t('BlogExtras.Avatar_Title', 'Author Avatar')
                )
            ]
        );

        $image = $fields->dataFieldByName('FeaturedImage');
        $image->setDescription(
            _t(
                'BlogExtras.FeaturedImage_Description',
                'Use images of 1920 x 400 for best results.'
            )
        );

        if ($this->owner->exists()) {
            $fields->addFieldsToTab(
                'Root.Related',
                [
                    GridField::create(
                        'RelatedPosts',
                        _t('BlogExtras.RelatedPosts_Title', 'Related Posts'),
                        $this->owner->RelatedPosts(),
                        GridFieldConfig_RelationEditor::create()->addComponent(
                            new GridFieldOrderableRows('SortOrder')
                        )
                    )
                ]
            );
        }

        $imageUpload->getValidator()->setAllowedExtensions(array('jpg', 'svg', 'png', 'jpeg'));
    }

    /**
     * Returns a sorted list of the related posts.
     *
     * @return DataList
     */
    public function getSortedRelatedPosts()
    {
        return $this->owner->RelatedPosts()->exclude(array('ID' => $this->owner->ID))->sort('SortOrder');
    }

    /**
     * Returns a list of this post's siblings - that is, children of this post's parent.  For use in templates as $Siblings.
     *
     * Will be sorted by date, most recent first.
     *
     * @return DataList
     */
    public function getSiblings()
    {
        return BlogPost::get()->filter('ParentID', $this->owner->ParentID)->sort('PublishDate', 'DESC');
    }

    public function canView($member = null)
    {
        return true;
    }
}
