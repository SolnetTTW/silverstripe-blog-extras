<?php

namespace Solnet\Blog;

use Solnet\Blog\BlogPost;
use SilverStripe\Core\Config\Config;
use SilverStripe\Blog\Model\BlogPostFilter as SilverStripeBlogPostFilter;
use SilverStripe\Admin\LeftAndMain;
use SilverStripe\Control\Controller;
use SilverStripe\Core\Convert;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\DataQuery;
use SilverStripe\ORM\FieldType\DBDatetime;
use SilverStripe\ORM\Queries\SQLSelect;
use SilverStripe\Security\Permission;
use SilverStripe\Versioned\Versioned;

/**
 * This is responsible for filtering only published posts to users who do not have permission to
 * view non-published posts.
 *
 */
class BlogPostFilter extends SilverStripeBlogPostFilter
{
    /**
     * Augment queries so that we don't fetch unpublished articles.
     *
     * @param SQLSelect $query
     * @param DataQuery $query
     */
    public function augmentSQL(SQLSelect $query, DataQuery $dataQuery = null)
    {
        $stage = Versioned::get_stage();

        if (Controller::has_curr() && Controller::curr() instanceof LeftAndMain) {
            return;
        }

        $hide_future_blog_posts = Config::inst()->get(BlogPost::class, 'hide_future_publish_date');
        if ($hide_future_blog_posts && ($stage == 'Live' || !Permission::check('VIEW_DRAFT_CONTENT'))) {
            $query->addWhere(sprintf(
                '"PublishDate" < \'%s\'',
                Convert::raw2sql(DBDatetime::now())
            ));
        }
    }
}
