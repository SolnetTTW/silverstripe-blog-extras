<?php

namespace Solnet\Blog;

use SilverStripe\ORM\DB;
use SilverStripe\Dev\BuildTask;

class FixBlogPostClassNames extends BuildTask
{

    protected $title = 'Fix BlogPost ClassName values';

    protected $description = 'Repairs BlogPost entries in the database that have incorrect ClassName values.';

    public function run($request)
    {
        DB::query(
            "UPDATE \"SiteTree\" SET \"ClassName\" = 'Solnet\\\\Blog\\\\BlogPost' ".
            "WHERE \"ClassName\" = 'SilverStripe\\\\Blog\\\\Model\\\\BlogPost'"
        );
        DB::alteration_message("Updated ".DB::affected_rows()." rows in SiteTree table.", "repaired");

        DB::query(
            "UPDATE \"SiteTree_Live\" SET \"ClassName\" = 'Solnet\\\\Blog\\\\BlogPost' ".
            "WHERE \"ClassName\" = 'SilverStripe\\\\Blog\\\\Model\\\\BlogPost'"
        );
        DB::alteration_message("Updated ".DB::affected_rows()." rows in SiteTree_Live table.", "repaired");

        DB::query(
            "UPDATE \"SiteTree_Versions\" SET \"ClassName\" = 'Solnet\\\\Blog\\\\BlogPost' ".
            "WHERE \"ClassName\" = 'SilverStripe\\\\Blog\\\\Model\\\\BlogPost'"
        );
        DB::alteration_message("Updated ".DB::affected_rows()." rows in SiteTree_Versions table.", "repaired");

        DB::alteration_message("Done!", "repaired");
    }
}
