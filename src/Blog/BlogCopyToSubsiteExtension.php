<?php

namespace Solnet\Blog;

use SilverStripe\Blog\Model\Blog;
use SilverStripe\Control\Controller;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Subsites\Model\Subsite;
use SilverStripe\View\Requirements;

class BlogCopyToSubsiteExtension extends DataExtension
{
    public function updateCMSFields(FieldList $fields)
    {
        Requirements::javascript('solnet/silverstripe-blog-extras:client/dist/js/BlogPostCMS.js');

        // Create source; disable subsite filter so we get all of them
        $source = [];
        $originalSubsiteFilter = Subsite::$disable_subsite_filter;
        Subsite::disable_subsite_filter(true);
        $blogs = Blog::get()->sort('SubsiteID, Title');
        foreach ($blogs as $blog) {
            if (!array_key_exists($blog->SubsiteID, $source)) {
                $source[$blog->SubsiteID] = [];
            }
            $source[$blog->SubsiteID][$blog->ID] = $blog->Title;
        }
        Subsite::disable_subsite_filter($originalSubsiteFilter);

        // Add field
        $fields->insertAfter(
            'CopyToSubsiteID',
            $copyToSubsiteParentID = DropdownField::create(
                'CopyToSubsiteParentID',
                _t('BlogExtras.CopyToSubsiteParentID_Title', 'Copy into Blog'),
                []
            )
        );
        $copyToSubsiteParentID->setAttribute('data-options', json_encode($source, JSON_FORCE_OBJECT));
        $copyToSubsiteParentID->setDescription(
            _t(
                'BlogExtras.CopyToSubsiteParentID_Description',
                'Select a parent Blog to copy your Post into'
            )
        );
    }

    /**
     * Sets Parent ID according to incoming GET parameter CopyToSubsiteParentID (if given)
     *
     * @param SiteTree $original
     */
    public function onBeforeDuplicateToSubsite($original)
    {
        $parentID = Controller::curr()->getRequest()->getParam('CopyToSubsiteParentID');
        if ($parentID) {
            $this->owner->ParentID = $parentID;
        }
    }
}
