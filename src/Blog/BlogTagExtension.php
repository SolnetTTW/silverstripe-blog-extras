<?php

namespace Solnet\Blog;

use SilverStripe\ORM\DataExtension;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\FieldList;

class BlogTagExtension extends DataExtension
{
    private static $has_one = [
        'TagIcon' => 'SilverStripe\Assets\Image'
    ];

    public function updateCMSFields(FieldList $fields)
    {

        $fields->addFieldsToTab(
            'Root.Main',
            [
                $imageUpload = UploadField::create(
                    'TagIcon',
                    _t('BlogExtras.TagIcon_Title', 'Tag Icon')
                )
            ]
        );

        $imageUpload->getValidator()->setAllowedExtensions(array('jpg', 'svg', 'png', 'jpeg'));
    }
}
