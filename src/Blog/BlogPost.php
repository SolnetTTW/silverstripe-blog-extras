<?php

namespace Solnet\Blog;

use SilverStripe\Core\Config\Configurable;
use SilverStripe\Blog\Model\BlogPost as SilverStripeBlogPost;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Versioned\Versioned;

class BlogPost extends SilverStripeBlogPost
{
    use Configurable;

    /**
     * @config
     * Sets whether BlogPosts with a future publish date are visible to the general public.
     */
    private static $hide_future_publish_date = true;

    /**
     * Replaces BlogPost::canView and removes the feature where articles with a future PublishDate
     * cannot be viewed by anonynmous users.
     */
    public function canView($member = null)
    {
        $member = $this->getMember($member);

        // Little-known PHP feature;
        // SiteTree::canView in this case will have $this in scope, even though it looks like
        // it is being called as a static method.
        // This line of code replaces parent::canView(), which would otherwise have called the
        // BlogPost::canView() method we are trying to replace here.
        if (!SiteTree::canView($member)) {
            return false;
        }

        if ($this->canEdit($member)) {
            return true;
        }

        // If on draft stage, user has permission to view draft, so show it
        if (Versioned::get_stage() === Versioned::DRAFT) {
            return true;
        }

        // Skip checking the publish date?
        if ($this->config()->get('hide_future_publish_date')) {
            /**
             * @var DBDatetime $publishDate
             */
            $publishDate = $this->dbObject('PublishDate');
            if (!$publishDate->exists()) {
                return false;
            }

            return !$publishDate->InFuture();
        }

        return true;
    }
}
