<?php

namespace Solnet\Blog;

use Solnet\Blog\BlogPost;
use SilverStripe\Core\Config\Config;
use SilverStripe\Blog\Model\BlogFilter as SilverStripeBlogFilter;
use SilverStripe\Lumberjack\Model\Lumberjack;

/**
 * Changes whether we can see Blog Posts with future PublisDate values.
 */
class BlogFilter extends SilverStripeBlogFilter
{

    public function stageChildren($showAll = false)
    {
        // Lumberjack is the parent's parent - so we skip running the code inside SilverStripe\Blog\Model\BlogFilter,
        // which we are trying to replace here.
        if (Config::inst()->get(BlogPost::class, 'hide_future_publish_date')) {
            return Lumberjack::stageChildren($showAll);
        } else {
            return parent::stageChildren($showAll);
        }
    }

    public function liveChildren($showAll = false, $onlyDeletedFromStage = false)
    {
        // Lumberjack is the parent's parent - so we skip running the code inside SilverStripe\Blog\Model\BlogFilter,
        // which we are trying to replace here.
        if (Config::inst()->get(BlogPost::class, 'hide_future_publish_date')) {
            return Lumberjack::liveChildren($showAll, $onlyDeletedFromStage);
        } else {
            return parent::liveChildren($showAll, $onlyDeletedFromStage);
        }
    }
}
